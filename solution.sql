Microsoft Windows [Version 10.0.19044.2604]
(c) Microsoft Corporation. All rights reserved.

C:\Users\ej nuneza>mysql -u root
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 305
Server version: 10.4.27-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> USE music_db
Database changed
MariaDB [music_db]> SELECT * FROM artists WHERE name LIKE '%D%';
+----+---------------+
| id | name          |
+----+---------------+
|  4 | Lady Gaga     |
|  6 | Ariana Grande |
+----+---------------+
2 rows in set (0.000 sec)

MariaDB [music_db]>  SELECT * FROM songs WHERE length < 230;
+----+------------+----------+-------------------------+----------+
| id | song_name  | length   | genre                   | album_id |
+----+------------+----------+-------------------------+----------+
|  3 | Pardon Me  | 00:02:23 | Rock                    |        1 |
|  4 | Stellar    | 00:02:00 | Rock                    |        1 |
|  6 | Love Story | 00:02:13 | Country Pop             |        3 |
|  8 | Red        | 00:02:04 | Country                 |        4 |
|  9 | Black Eyes | 00:00:00 | Rock and roll           |        5 |
| 10 | Shallow    | 00:02:01 | Country, rock,folk rock |        5 |
| 12 | Sorry      | 00:01:32 | Dancehall-poptropical   |        7 |
| 13 | 24k Magic  | 00:02:07 | Funk, disco, R&B        |       11 |
| 14 | Lost       | 00:01:52 | Pop                     |       12 |
+----+------------+----------+-------------------------+----------+
9 rows in set (0.000 sec)

MariaDB [music_db]>  SELECT albums.name AS album_name, songs.name AS song_name, songs.length FROM albums JOIN songs ON albums.id = songs.album_id;
ERROR 1054 (42S22): Unknown column 'albums.name' in 'field list'
MariaDB [music_db]> SELECT * FROM albums;
+----+-----------------+---------------+-----------+
| id | album_title     | date_released | artist_id |
+----+-----------------+---------------+-----------+
|  1 | Make Yourself   | 1999-10-26    |         1 |
|  2 | Psy 6           | 2012-01-15    |         2 |
|  3 | Fearless        | 2008-11-11    |         3 |
|  4 | Red             | 2010-10-22    |         3 |
|  5 | A Star Is Born  | 2018-10-10    |         4 |
|  6 | Born This way   | 2011-06-29    |         4 |
|  7 | Purpose         | 2015-11-13    |         5 |
|  8 | Believe         | 2016-06-15    |         5 |
|  9 | Dangerous Woman | 2016-05-20    |         6 |
| 10 | Thank U, Next   | 2019-02-08    |         6 |
| 11 | 24k Magic       | 2016-11-18    |         7 |
| 12 | Earth to Mars   | 2011-01-20    |         7 |
+----+-----------------+---------------+-----------+
12 rows in set (0.000 sec)

MariaDB [music_db]> SELECT * FROM songs;
+----+----------------+----------+------------------------------------+----------+
| id | song_name      | length   | genre                              | album_id |
+----+----------------+----------+------------------------------------+----------+
|  1 | Gangnam Style  | 00:02:53 | K-POP                              |        2 |
|  3 | Pardon Me      | 00:02:23 | Rock                               |        1 |
|  4 | Stellar        | 00:02:00 | Rock                               |        1 |
|  5 | Fearless       | 00:02:46 | Pop rock                           |        3 |
|  6 | Love Story     | 00:02:13 | Country Pop                        |        3 |
|  7 | State of Grace | 00:02:43 | Rock, alternative rock, arena rock |        4 |
|  8 | Red            | 00:02:04 | Country                            |        4 |
|  9 | Black Eyes     | 00:00:00 | Rock and roll                      |        5 |
| 10 | Shallow        | 00:02:01 | Country, rock,folk rock            |        5 |
| 11 | Born This Way  | 00:02:52 | Electropop                         |        6 |
| 12 | Sorry          | 00:01:32 | Dancehall-poptropical              |        7 |
| 13 | 24k Magic      | 00:02:07 | Funk, disco, R&B                   |       11 |
| 14 | Lost           | 00:01:52 | Pop                                |       12 |
+----+----------------+----------+------------------------------------+----------+
13 rows in set (0.000 sec)

MariaDB [music_db]> SELECT albums.name AS album_title, songs.name AS song_name, songs.length FROM albums JOIN songs ON albums.id = songs.album_id;
ERROR 1054 (42S22): Unknown column 'albums.name' in 'field list'
MariaDB [music_db]> SELECT albums.title AS album_title, songs.name AS song_name, songs.length FROM albums JOIN songs ON albums.id = songs.album_id;
ERROR 1054 (42S22): Unknown column 'albums.title' in 'field list'
MariaDB [music_db]> SELECT albums.album_title, songs.song_name, songs.length FROM albums
    -> INNER JOIN songs ON albums.id = songs.album_id;
+----------------+----------------+----------+
| album_title    | song_name      | length   |
+----------------+----------------+----------+
| Psy 6          | Gangnam Style  | 00:02:53 |
| Make Yourself  | Pardon Me      | 00:02:23 |
| Make Yourself  | Stellar        | 00:02:00 |
| Fearless       | Fearless       | 00:02:46 |
| Fearless       | Love Story     | 00:02:13 |
| Red            | State of Grace | 00:02:43 |
| Red            | Red            | 00:02:04 |
| A Star Is Born | Black Eyes     | 00:00:00 |
| A Star Is Born | Shallow        | 00:02:01 |
| Born This way  | Born This Way  | 00:02:52 |
| Purpose        | Sorry          | 00:01:32 |
| 24k Magic      | 24k Magic      | 00:02:07 |
| Earth to Mars  | Lost           | 00:01:52 |
+----------------+----------------+----------+
13 rows in set (0.000 sec)

MariaDB [music_db]> SELECT * FROM artists;
+----+---------------+
| id | name          |
+----+---------------+
|  1 | Incubus       |
|  2 | Psy           |
|  3 | Taylor Swift  |
|  4 | Lady Gaga     |
|  5 | Justin Bieber |
|  6 | Ariana Grande |
|  7 | Bruno Mars    |
+----+---------------+
7 rows in set (0.000 sec)

MariaDB [music_db]> SELECT albums.album_title FROM artists INNER JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE '%A%';
+-----------------+
| album_title     |
+-----------------+
| Make Yourself   |
| Fearless        |
| A Star Is Born  |
| Born This way   |
| Dangerous Woman |
| Thank U, Next   |
| 24k Magic       |
| Earth to Mars   |
+-----------------+
8 rows in set (0.000 sec)

MariaDB [music_db]> SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;
+----+---------------+---------------+-----------+
| id | album_title   | date_released | artist_id |
+----+---------------+---------------+-----------+
| 10 | Thank U, Next | 2019-02-08    |         6 |
|  4 | Red           | 2010-10-22    |         3 |
|  7 | Purpose       | 2015-11-13    |         5 |
|  2 | Psy 6         | 2012-01-15    |         2 |
+----+---------------+---------------+-----------+
4 rows in set (0.000 sec)

MariaDB [music_db]> SELECT albums.album_title, songs.song_name, songs.length FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY albums.album_title DESC, songs.song_name ASC;
+----------------+----------------+----------+
| album_title    | song_name      | length   |
+----------------+----------------+----------+
| Red            | Red            | 00:02:04 |
| Red            | State of Grace | 00:02:43 |
| Purpose        | Sorry          | 00:01:32 |
| Psy 6          | Gangnam Style  | 00:02:53 |
| Make Yourself  | Pardon Me      | 00:02:23 |
| Make Yourself  | Stellar        | 00:02:00 |
| Fearless       | Fearless       | 00:02:46 |
| Fearless       | Love Story     | 00:02:13 |
| Earth to Mars  | Lost           | 00:01:52 |
| Born This way  | Born This Way  | 00:02:52 |
| A Star Is Born | Black Eyes     | 00:00:00 |
| A Star Is Born | Shallow        | 00:02:01 |
| 24k Magic      | 24k Magic      | 00:02:07 |
+----------------+----------------+----------+
13 rows in set (0.000 sec)

MariaDB [music_db]>